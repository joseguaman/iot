var express = require('express');
var router = express.Router();
var rp = require('request-promise');
var URL = "http://10.32.23.170/";//url de la mota 1
var URL1 = "http://10.32.23.171/";//url de la mota 2

/* GET home page. */
router.get('/foco/uno/estado', function (req, res, next) {
    rp(URL + '2')
            .then(function (htmlString) {
                var datos = JSON.parse(htmlString);
                res.send(datos);
            })
            .catch(function (err) {
                // Crawling failed...
                res.send({name: err.name, status: 404});
            });
});
router.get('/foco/uno/encender', function (req, res, next) {
    rp(URL + '1')
            .then(function (htmlString) {
                var datos = JSON.parse(htmlString);
                res.send(datos);
            })
            .catch(function (err) {
                // Crawling failed...
                res.send({name: err.name, status: 404});
            });
});
router.get('/foco/uno/apagar', function (req, res, next) {
    rp(URL + '0')
            .then(function (htmlString) {
                var datos = JSON.parse(htmlString);
                res.send(datos);
            })
            .catch(function (err) {
                // Crawling failed...
                console.log(err);
                res.send({name: err.name, status: 404});
            });
});

router.get('/temp/estado', function (req, res, next) {
    rp(URL1 + '3')
            .then(function (htmlString) {
                var datos = JSON.parse(htmlString);
                res.send(datos);
            })
            .catch(function (err) {
                // Crawling failed...
                console.log(err);
                res.send({name: err.name, status: 404});
            });
});

router.get('/estado/casa', function (req, res, next) {
    rp(URL + '2')
            .then(function (htmlString) {
                //var datos = JSON.parse(htmlString);
                //res.send(datos);
                rp(URL1 + '3')
                        .then(function (htmlStringT) {
                            var datos = JSON.parse(htmlString);
                            var datosT = JSON.parse(htmlStringT);
                            var datosTotales = {estadoF:datos.estado, estadoFT: datosT.estado, temp: datosT.temperatura};
                            res.json(datosTotales);
                        })
                        .catch(function (err) {
                            // Crawling failed...
                            console.log(err);
                            res.json({name: err.name, status: 404});
                        });
            })
            .catch(function (err) {
                // Crawling failed...
                res.json({name: err.name, status: 404});
            });
});

module.exports = router;
