//importamos la libreria wifi
#include <WiFi.h>
//https://www.youtube.com/watch?v=f3UyU0up8OE
//Asignamos un puerto de escucha a nuestro servidor web
WiFiServer server(80);
const char* ssid = "NOMBRE DE LA RED WIFI";

const char* password =  "CLAVE DE LA RED WIFI";
//Variable donde se almacena los datos que nos vienen en la cabecera
String cabecera;
//El tiempo de conexion a la red wifi
int conexion = 0;
//el dato que se espera de la peticion http: 0 siginifica apagado, 1 significa prendido
String estadoSalida = "0";
String valorTemp = "0";
String valorTempInt = "0";
//la salida de nuestro circuito
const int salida = 4;
//GPIO para leer el sensor de temperatuta
const int out_tempe = A0;
//Nuestra pagina web html
String html = "";

void setup() {
  //iniciamos la velocidad de comunicacion
  Serial.begin(115200);
  Serial.write("El ESP32 se esta iniciando.");
  delay(500);
  pinMode(salida, OUTPUT);
  digitalWrite(salida, LOW);
  //inicializamos nuestra conexion wifi a la red de datos
  WiFi.begin(ssid, password);

  //mientras se conecta o han pasado 30 seg en la conexion
  while (WiFi.status() != WL_CONNECTED and conexion < 60) {
    conexion ++;
    delay(500);
    Serial.println("Conectando a la red ..");
  }
  if (conexion <= 50) {
    //para fijar IP
    //para fijar IP
    IPAddress ip(10, 32, 23, 171);//IPAddress ip(192, 168, 1, 151);
    //Para fijar la puerta de entrada
      IPAddress gateway(10, 32, 23, 1);//IPAddress gateway(192, 168, 1, 1);
      //Para fijar la mascara de sub red
      IPAddress subnet(255, 255, 224, 0);//IPAddress subnet(255, 255, 255, 0);
    //Para configurar todo
    WiFi.config(ip, gateway, subnet);

    Serial.println(".....");
    Serial.println("...WIFI CONECTADO..");
    Serial.println(WiFi.localIP());
    pinMode(2, OUTPUT); //Configuramos el led del hardware de la placa
    digitalWrite(2, HIGH);//encendemos el led
    WiFi.setAutoReconnect(true);
    server.begin();
  }
  else {
    Serial.println("ERROR DE CONEXION");
  }
}

void loop() {
  Serial.write("Se comienza");
  delay(100);
  WiFiClient client = server.available();   // Escucha a los clientes entrantes

  if (client) {                             // Si se conecta un nuevo cliente
    Serial.println("Nuevo cliente.");          //
    String currentLine = "";                //
    html = htmlData();
    while (client.connected()) {            // loop mientras el cliente está conectado
      if (client.available()) {             // si hay bytes para leer desde el cliente
        char c = client.read();             // lee un byte
        Serial.write(c);                    // imprime ese byte en el monitor serial
        cabecera += c;
        if (c == '\n') {                    // si el byte es un caracter de salto de linea
          // si la nueva linea está en blanco significa que es el fin del
          // HTTP request del cliente, entonces respondemos:
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            // cabecera y apaga el GPIO
            if (cabecera.indexOf("GET /1") >= 0) {
                            
              html = htmlData();
            }  else if (cabecera.indexOf("GET /3") >= 0) {
              html = htmlJsonTem();

            } else {
              html = htmlData();
              }

            // Muestra la página web

            client.println(html);

            // la respuesta HTTP temina con una linea en blanco
            client.println();
            break;
          } else { // si tenemos una nueva linea limpiamos currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // si C es distinto al caracter de retorno de carro
          currentLine += c;      // lo agrega al final de currentLine
        }
      }
    }
    // Limpiamos la variable header
    cabecera = "";
    // Cerramos la conexión
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

float temperatura() {
  int rawvoltage = analogRead(out_tempe);
  float aux = (( 5.0 * rawvoltage * 100.0) / 1023.0);
  
  for(int i  = 0;i<=4;i++){ // realizara 5 iteraciones
  rawvoltage = analogRead(out_tempe);
  aux = aux + (( 5.0 * rawvoltage * 100.0) / 1023.0); //iteraciones entre resolucion
  
  delay(100);                //retardo para realizar de nuevo el muestreo

}

  //Serial.println("******* "+rawvoltage);
  //valorTempInt = String(rawvoltage);
  //float millivolts = (rawvoltage / 2048.0) * 3300;//(rawvoltage * 5000.0) / 1024.0;
  //float temperatureC = millivolts * 0.1;//millivolts / 10;
  float temperatureC = aux/5;
  float temperatureF = (temperatureC * 1.8) + 32;
  //float fahrenheit = millivolts / 10;  
  //float celsius = (fahrenheit - 32) * (5.0 / 9.0);
  if(temperatureC >= 16) {
    digitalWrite(salida, HIGH);//encendemos el led
    estadoSalida = "1";
    } else {
      estadoSalida = "0";
      digitalWrite(salida, LOW);//encendemos el led
      }
  
  return temperatureC;
}
String htmlData() {
  float v = temperatura();
  valorTemp = String(v) + " oC";


  html = "<!DOCTYPE html>"
         "<html>"
         "<head>"
         "<meta charset='utf-8' />"
         "<title>Servidor Web</title>"
         "</head>"
         "<body>"
         "<center>"
         "<h1>Servidor Web con la placa ESP32</h1>"         
         "<p>TEMPERATURA " + valorTemp + "</p>"
         "</center>"
         "</body>"
         "</html>";
  return html;
}
String htmlJsonTem() {
  //"{ \"estado\": \""+estado+"\"}"
  html = "{ \"estado\": \""+estadoSalida+"\""+","+ "\"temperatura\": \""+valorTemp+"\"}";
  //html = "{ \"estado\": \""+salida+"\", \"temperatura\": \""+valorTemp+"\"}";
  
    return html;
}
