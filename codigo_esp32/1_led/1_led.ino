//importamos la libreria wifi
#include <WiFi.h>
//Asignamos un puerto de escucha a nuestro servidor web
WiFiServer server(80);
const char* ssid = "NOMBRE DE LA RED WIFI";

const char* password =  "CLAVE DE LA RED WIFI";
//Variable donde se almacena los datos que nos vienen en la cabecera
String cabecera;
//El tiempo de conexion a la red wifi
int conexion = 0;
//el dato que se espera de la peticion http: 0 siginifica apagado, 1 significa prendido
String estadoSalida = "0";
//la salida de nuestro circuito
const int salida = 4;
const int salidaR = 5;
//Nuestra pagina web html
String html = "";

void setup() {
  //iniciamos la velocidad de comunicacion 
  Serial.begin(115200);
  Serial.write("El ESP32 se esta iniciando.");
  delay(500);
  pinMode(salida, OUTPUT);
  digitalWrite(salida, LOW);
  pinMode(salidaR, OUTPUT);
  digitalWrite(salidaR, HIGH);
  //inicializamos nuestra conexion wifi a la red de datos
  WiFi.begin(ssid, password);

  //mientras se conecta o han pasado 30 seg en la conexion
  while (WiFi.status() != WL_CONNECTED and conexion < 60) {
    conexion ++;
    delay(500);
    Serial.println("Conectando a la red ..");
  }
  if (conexion <= 50) {
    //para fijar IP
    IPAddress ip(10, 32, 23, 170);//IPAddress ip(192, 168, 1, 150);
    //Para fijar la puerta de entrada
      IPAddress gateway(10, 32, 23, 1);//IPAddress gateway(192, 168, 1, 1);
      //Para fijar la mascara de sub red
      IPAddress subnet(255, 255, 224, 0);//IPAddress subnet(255, 255, 255, 0);
      //Para configurar todo
      WiFi.config(ip, gateway, subnet);

    Serial.println(".....");
    Serial.println("...WIFI CONECTADO..");
    Serial.println(WiFi.localIP());
    pinMode(2, OUTPUT); //Configuramos el led del hardware de la placa
    digitalWrite(2, HIGH);//encendemos el led
    //pinMode(salida, OUTPUT); //Configuramos el led del hardware de la placa
    //digitalWrite(salida, LOW);//encendemos el led
    server.begin();
  }
  else {    
    Serial.println("ERROR DE CONEXION");
  }  
  html = html_pag();
}

void loop(){
  Serial.write("Se comienza");
  delay(100);
  WiFiClient client = server.available();   // Escucha a los clientes entrantes

  if (client) {                             // Si se conecta un nuevo cliente
    Serial.println("Nuevo cliente.");          // 
    String currentLine = "";                //
    while (client.connected()) {            // loop mientras el cliente esta conectado
      if (client.available()) {             // si hay bytes para leer desde el cliente
        char c = client.read();             // lee un byte
        Serial.write(c);                    // imprime ese byte en el monitor serial
        cabecera += c;
        if (c == '\n') {                    // si el byte es un caracter de salto de linea
          // si la nueva linea esta en blanco significa que es el fin del 
          // HTTP request del cliente, entonces respondemos:
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // cabecera y apaga el GPIO
            if (cabecera.indexOf("GET /1") >= 0) {
              Serial.println("GPIO 1");
              estadoSalida = "1";
              html = respuesta(estadoSalida);
              digitalWrite(salida, HIGH);
              digitalWrite(salidaR, LOW);//encendemos el led
            } else if (cabecera.indexOf("GET /0") >= 0) {
              Serial.println("GPIO 0");
              estadoSalida = "0";
              digitalWrite(salida, LOW);
              digitalWrite(salidaR, HIGH);
              html = respuesta(estadoSalida);
            } else if (cabecera.indexOf("GET /2") >= 0) {              
              html = respuesta(estadoSalida);
            } else {
              html = html_pag();
              }
                       
            // Muestra la pagina web
            client.println(html);
            
            // la respuesta HTTP temina con una linea en blanco
            client.println();
            break;
          } else { // si tenemos una nueva linea limpiamos currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // si C es distinto al caracter de retorno de carro
          currentLine += c;      // lo agrega al final de currentLine
        }
      }
    }
    // Limpiamos la variable header
    cabecera = "";
    // Cerramos la conexion
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
  
}
String html_pag() {
  html = "<!DOCTYPE html>"
"<html>"
"<head>"
"<meta charset='utf-8' />"
"<title>Servidor Web</title>"
"</head>"
"<body>"
"<center>"
"<h1>Servidor Web con la placa ESP32</h1>"
"<p><a href='/1'><button style='height:75px;width:100px'>ENCENDER</button></a></p>"
"<p><a href='/0'><button style='height:75px;width:100px'>APAGAR</button></a></p>"
"</center>"
"</body>"
"</html>";
return html;
}
String respuesta (String estado) {
  html = "{ \"estado\": \""+estado+"\"}";
  return html;
  }
